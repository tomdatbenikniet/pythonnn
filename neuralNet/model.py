import numpy as np
from neuralNet.activations import Activations

from neuralNet.backwards import backwards_sigmoid, backwards_softmax
from neuralNet.forward import forwards_sigmoid, forwards_softmax


class LisModel():
    def __init__(self, input_size: int, output_size: int, hidden_layers: int, activation: Activations):
        self.input_size = input_size
        self.output_size = output_size
        self.init_layers(input_size, output_size, hidden_layers)
        self.activation = activation
        self.neuron_values = []

    def init_layers(self, input_size, output_size, hidden_layers):
        self.layers = []
        self.bias = []

        hidden_layers_size = round(self.input_size/3*2+self.output_size)

        self.layers.append(np.random.rand(input_size, hidden_layers_size))
        self.bias.append(np.random.randn(hidden_layers_size))

        for i in range(hidden_layers):
            self.bias.append(np.random.randn(hidden_layers_size))

            if i == hidden_layers - 1:
                self.layers.append(np.random.rand(
                    hidden_layers_size, output_size))
            else:
                hiddenNextSize = round(hidden_layers_size/3*2+output_size)
                self.layers.append(np.random.rand(
                    hidden_layers_size, hiddenNextSize))
                hidden_layers_size = hiddenNextSize

    def forward(self, data, training: bool = True):
        if self.activation == Activations.SIGMOID:
            return forwards_sigmoid(self, data, training)
        elif self.activation == Activations.SOFTMAX:
            return forwards_softmax(self, data, training)

    def backwards(self, example, output, training: bool = True):
        if self.activation == Activations.SIGMOID:
            return backwards_sigmoid(self, example, output, training)
        elif self.activation == Activations.SOFTMAX:
            return backwards_softmax(self, example, output, training)

    def adjust(self, deltas):
        for i in range(len(self.layers)):
            layer = self.layers[i]
            delta = deltas[i]
            nuronValue = self.neuron_values[i]
            layer += np.matmul(np.matrix.transpose(nuronValue), delta)
