from neuralNet.activations import sigmoid
import numpy as np


def forwards_sigmoid(model, input, training):
    neuron_values = [input]
    result = input

    for i in range(len(model.layers)):
        # format input * weights
        # matmul = np.matmul(result, model.layers[i])

        # activation function
        result = sigmoid(np.dot(result, model.layers[i]))

        # activations
        if training:
            neuron_values.append(result)

    model.neuron_values = neuron_values
    return result


def forwards_softmax(model, data, training):
    steps = []
    print('not implemented')

    return steps
