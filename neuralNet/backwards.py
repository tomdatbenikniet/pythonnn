from neuralNet.activations import sigmoidPrime
import numpy as np


def backwards_sigmoid(model, example, output, training):
    errors = []
    deltas = []

    layers = len(model.layers)

    model.neuron_values.reverse()
    forward_steps = model.neuron_values
    model.neuron_values.reverse()

    for i in range(layers):
        if i == 0:

            deltas.append(
                np.dot(forward_steps[layers - i].T, 2*(example - output) * sigmoidPrime(output)))
            # errors.append(example - output)
            # deltas.append(errors[i] * sigmoidPrime(output))
        else:
            deltas.append(np.dot(forward_steps[layers - i].T, np.dot(2*(example - output)*sigmoidPrime(
                output), model.layers[layers - i].T)*sigmoidPrime(model.layers[i])))

            # delta = deltas[i-1]
            # layer = model.layers[layers - i]

            # transposedLayer = np.matrix.transpose(layer)

            # error = np.matmul(delta, transposedLayer)
            # errors.append(error)

            # activation = forward_steps[i]

            # deltas.append(error * sigmoidPrime(activation))

    deltas.reverse()

    return deltas


def backwards_softmax(model, forward_steps, data):
    print('not implemented')
