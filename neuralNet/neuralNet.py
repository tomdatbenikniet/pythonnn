from neuralNet.activations import Activations, sigmoid
from neuralNet.model import LisModel
import numpy as np
import matplotlib.pyplot as plt

class NeuralNet():
    def __init__(self, input_size: int, output_size: int, hidden_layers: int, activation: Activations):
        self.model = LisModel(input_size, output_size,
                              hidden_layers, activation)
        self.error_list = []

    def train(self, data, example, epochs: int):
        for e in range(epochs):
            progress = str(round(100 / epochs * e))
            if e == 0:
                progress = '0'

            print('epoch process: ' + progress + "%")

            o = self.model.forward(data)
   
            deltas = self.model.backwards(example, o)

            self.error_list.append(np.abs(example - o).mean())

            self.model.adjust(deltas)

    def predict(self,data):
        if self.model.activation == Activations.SIGMOID:
            return self.model.forward(data).item()
        elif self.model.activation == Activations.SOFTMAX:
            return self.model.forward(data)

    def display_training_session(self):
            plt.plot(range(len(self.error_list)), self.error_list)
            plt.title('Mean Sum Squared Loss')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.show()

