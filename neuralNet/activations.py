import enum
import numpy as np
from enum import Enum

class Activations(Enum):
    SIGMOID = 1
    SOFTMAX = 2


def sigmoid(v):
    return 1 / (1 + np.exp(-v))


def softmax(v):
    exps = np.exp(v - np.max(v))
    return exps / np.sum(exps)

def sigmoidPrime(v):
    return v * (1 - v)
