from examples.imgLoader import ImageData


def get_label_bag(labels, imageData: ImageData):
    data = []

    for image in imageData:
        data.append(bag(labels, image.label))

    return data


def bag(labels, label):
    bag = [0]*len(labels)
    for i, l in enumerate(labels):
        if l == label:
            # assign 1 if current word is in the vocabulary position
            bag[i] = 1
    return bag


def unpack_bag(bag, labels):
    label = ''
    for i, in len(bag):
        if bag[i] == 1:
            label = labels[i]
            break
    return label
