
class Settings():
    def __init__(self):
        self.train_path = "D:/Datasets/Fruit/fruits-360_dataset/fruits-360/Training"
        self.validate_path = "D:/Datasets/Fruit/fruits-360_dataset/fruits-360/Test"
        self.labels = ["Banana", "Apple Granny Smith", "Apple Braeburn"]
        self.channels = 3