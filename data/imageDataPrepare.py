import numpy as np
from sklearn.preprocessing import MinMaxScaler
from data.bag import get_label_bag
from data.imgData import ImageData
from PIL import Image
import glob

from data.settings import Settings

class ImgDataTransformer():
    def __init__(self, settings: Settings):
        self.settings = settings

    def get_image_input_data(self, settings):
        return self.load_images_with_labels(self.settings.labels, settings.training_path, self.settings.width, self.settings.height)

    def get_image_output_data(imageData, labels):
        return np.array(get_label_bag(labels, imageData))

    def get_data_input_size(self):
        return self.settings.channels * self.settings.width * self.settings.height

    def get_data_output_size(self):
        return len(self.settings.labels)

    def load_images_with_labels(self):
        image_list = []

        for label in self.settings.labels:
            for filename in glob.glob(self.settings.train_path + '/' + label + '/*.jpg'):  # assuming gif
                im = Image.open(filename)
                im = im.resize((self.settings.width, self.settings.height))
                image_list.append(ImageData(im, label))

        return image_list

    def load_image(self, label, name):
        im = Image.open(self.settings.validate_path + '/' + label + '/'+ name + '.jpg')
        im = im.resize((self.settings.width, self.settings.height))

        return ImageData(im, label)
