import numpy as np
from sklearn.preprocessing import MinMaxScaler


class SimpleData():
    def __init__(self):
        input_train = np.array([[0, 1, 0], [0, 1, 1], [0, 0, 0],
                                [1, 0, 0], [1, 1, 1], [1, 0, 1]])
        output_train = np.array([[0], [0], [0], [1], [1], [1]])

        input_test = np.array([[1, 1, 1], [1, 0, 1], [1, 0, 0],
                            [0, 1, 1], [0, 0, 0], [0, 1, 0]])
        output_test = np.array([[1], [1], [1], [0], [0], [0]])

        self.scaler = MinMaxScaler()
        self.input_train = self.scaler.fit_transform(input_train)
        self.output_train = self.scaler.fit_transform(output_train)
        self.input_test = self.scaler.fit_transform(input_test)
        self.output_test = self.scaler.fit_transform(output_test)
        self.test_true_predict = np.array([1, 1, 0])
        self.test_false_predict = np.array([0, 1, 0])