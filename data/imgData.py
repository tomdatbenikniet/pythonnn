import string
from PIL import Image

class ImageData():
    def __init__(self, image: Image, label: string):
        pixels = list(image.getdata())
        self.pixel_data = []
        self.image = image

        if image.mode == "RGB":
            self.channels = 3
            for pixel in list(pixels):
                for i in range(len(list(pixel))):
                    p = 100 / 255 * pixel[i]
                    self.pixel_data.append(1/100*p)
        elif image.mode == "L":
            self.channels = 1
        else:
            print("Unknown mode: %s" % image.mode)

        self.label = label