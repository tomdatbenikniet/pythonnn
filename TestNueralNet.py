

from data.imageDataPrepare import ImgDataTransformer
from data.simpleData import SimpleData
from neuralNet.activations import Activations
from neuralNet.neuralNet import NeuralNet

NN1 = NeuralNet(3, 1, 1, Activations.SIGMOID)

simpleData = SimpleData()

NN1.train(simpleData.input_train, simpleData.output_train, 1000)

NN1.display_training_session()

print(NN1.predict(simpleData.test_true_predict))
# transformer = ImgDataTransformer()

# NN2 = NeuralNet(transformer.get_data_input_size(),
#                 transformer.get_data_output_size(), 3, Activations.SOFTMAX)

# NN2.train(transformer.get_image_input_data(),
#           transformer.get_image_output_data(), 10)
