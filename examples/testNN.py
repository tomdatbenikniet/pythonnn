from cProfile import label
from data.bag import get_label_bag
from visualize.drawneuralnet import draw_neural_net
from examples.imgLoader import load_image, load_images_with_labels
from examples.oldNet import OldNet
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt

input_train = np.array([[0, 1, 0], [0, 1, 1], [0, 0, 0],
                        [1, 0, 0], [1, 1, 1], [1, 0, 1]])
output_train = np.array([[0], [0], [0], [1], [1], [1]])
input_pred = np.array([1, 1, 0])

input_test = np.array([[1, 1, 1], [1, 0, 1], [1, 0, 0],
                       [0, 1, 1], [0, 0, 0], [0, 1, 0]])
output_test = np.array([[1], [1], [1], [0], [0], [0]])

scaler = MinMaxScaler()
input_train_scaled = scaler.fit_transform(input_train)
output_train_scaled = scaler.fit_transform(output_train)
input_test_scaled = scaler.fit_transform(input_test)
output_test_scaled = scaler.fit_transform(output_test)

path = "D:/Datasets/Fruit/fruits-360_dataset/fruits-360/Training"
labels = ["Banana", "Apple Granny Smith", "Apple Braeburn"]
width = 10
height = 10
imageData = load_images_with_labels(labels, path, width, height)
channels = 3


output_train = np.empty([len(imageData), len(labels)])

input = []
output = []
for data in imageData:
    input.append(data.pixel_data)

output_train = np.array(get_label_bag(labels, imageData))

print(len(output_train))

input_train = np.array(input)

input_size = channels * width * height
output_size = len(labels)

NN = OldNet(input_size, output_size, 5)

NN.train(input_train, output_train, 100)
NN.view_error_development()

test_path = 'D:/Datasets/Fruit/fruits-360_dataset/fruits-360/Test'
name = '3_100'

image = load_image(test_path, 'Apple Braeburn', name, width, height)

print(NN.predict(np.array(image.pixel_data)))
# for x in range(1):
#     NN.train(input_train_scaled, output_train_scaled, 100000)
#     NN.view_error_development()
#     NN.test_evaluation(input_test_scaled, output_test_scaled)

# print("Input")
# print([1, 1, 1])

# print("Layer input -> hidden ")
# print(NN.W1)

# print("Layer hidden -> output")
# print(NN.W2)

# fig=plt.figure(figsize=(12, 12))
# ax=fig.gca()
# ax.axis('off')

# draw_neural_net(ax, .1, .9, .1, .9, [
#                 NN.inputSize, NN.hiddenSize, NN.outputSize], NN)
