# Importing all necessary libraries
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
from keras.preprocessing.image import load_img
import numpy as np
from keras.callbacks import ModelCheckpoint, TensorBoard

from keras.models import load_model
 
model = load_model('model_saved.h5')

image = load_img('D:/Datasets/Fruit-small/test/Dates/4_100.jpg', target_size=(20, 20))
img = np.array(image)
img = img / 255.0
img = img.reshape(1,20,20,3)
label = np.arg(model.predict(img),axis=1)

res = model.predict(img)[0]

ERROR_THRESHOLD = 0.25
results = [[i, r] for i, r in enumerate(res) if r > ERROR_THRESHOLD]
# sort by strength of probability
results.sort(key=lambda x: x[1], reverse=True)
return_list = []
for r in results:
    return_list.append({"intent": fruit_list[r[0]], "probability": str(r[1])})

print(return_list)