from datetime import datetime
from xml.etree.ElementTree import tostring
import numpy as np
import matplotlib.pyplot as plt

from visualize.drawneuralnet import draw_neural_net


class OldNet():
    def __init__(self, input_size, output_size, hiddenLayers: int):
        self.inputSize = input_size
        self.outputSize = output_size
        self.hiddenSize = round(self.inputSize/3*2+self.outputSize)

        self.layers = []

        self.layers.append(np.random.rand(self.inputSize, self.hiddenSize))

        for i in range(hiddenLayers):
            if i == hiddenLayers - 1:
                self.layers.append(np.random.rand(
                    self.hiddenSize, self.outputSize))
            else:
                self.hiddenSizeOld = self.hiddenSize
                self.hiddenNextSize = round(
                    self.hiddenSize/3*2+self.outputSize)

                self.layers.append(np.random.rand(
                    self.hiddenSize, self.hiddenNextSize))

                self.hiddenSize = self.hiddenNextSize

        self.error_list = []
        self.limit = 0.5
        self.true_positives = 0
        self.false_positives = 0
        self.true_negatives = 0
        self.false_negatives = 0

    def forward(self, input):
        # # format input * weights
        # self.z = np.matmul(input, self.W1)
        # # activation function
        # self.z2 = self.sigmoid(self.z)
        # self.z3 = np.matmul(self.z2, self.W2)
        # o = self.sigmoid(self.z3)

        self.nuronValues = [input]
        result = input

        for i in range(len(self.layers)):
            # format input * weights
            matmul = np.matmul(result, self.layers[i])
            
            # activation function
            result = self.sigmoid(matmul)
 

            # activations
            if self.training:
                self.nuronValues.append(result)

        result = self.softmax(result)

        return result

    def sigmoid(self, s):
        return 1 / (1 + np.exp(-s))

    def softmax(self, X):
        exps = np.exp(X - np.max(X))
        return exps / np.sum(exps)

    def delta_cross_entropy(self, X, y):
        """
        X is the output from fully connected layer (num_examples x num_classes)
        y is labels (num_examples x 1)
            Note that y is not one-hot encoded vector. 
            It can be computed as y.argmax(axis=1) from one-hot encoded vectors of labels if required.
        """
        m = y.shape[0]
        grad = self.softmax(X)
        grad[range(m), y] -= 1
        grad = grad/m
        return grad

    def sigmoidPrime(self, s):
        return s * (1 - s)

    # Example == wanted/ true value to input

    def backward(self, example, output):
        errors = []
        deltas = []

        layers = len(self.layers)

        # # Output to hidden error W2
        # self.o_error = example - output
        # # Output Delta
        # self.o_delta = self.o_error * self.sigmoidPrime(output)

        # # Input to hidden W1
        # self.z2_error = np.matmul(self.o_delta, np.matrix.transpose(self.W2))
        # self.z2_delta = self.z2_error * self.sigmoidPrime(self.z2)

        self.nuronValues.reverse()

        for i in range(layers):
            if i == 0:
                errors.append(example - output)
                deltas.append(errors[i] * self.sigmoidPrime(output))
            else:
                delta = deltas[i-1]
                layer = self.layers[layers - i]

                transposedLayer = np.matrix.transpose(layer)

                error = np.matmul(delta, transposedLayer)
                errors.append(error)

                activation = self.nuronValues[i]

                deltas.append(error * self.sigmoidPrime(activation))

        self.nuronValues.reverse()
        # self.W1 += np.matmul(np.matrix.transpose(
        #     self.nuronValues[0]), self.z2_delta)
        # self.W2 += np.matmul(np.matrix.transpose(self.z2), self.o_delta)

        deltas.reverse()

        for i in range(layers):
            layer = self.layers[i]
            delta = deltas[i]
            nuronValue = self.nuronValues[i]
            layer += np.matmul(np.matrix.transpose(nuronValue), delta)

    def train(self, input, example, epochs):
        self.training = True
        for epoch in range(epochs):
            procent = str(round(100 / epochs * epoch))
            if epoch == 0:
                procent = '0'
            print('epoch process: ' + procent + "%")
            o = self.forward(input)
            self.backward(example, o)
            self.error_list.append(np.abs(example - o).mean())

    def predict(self, x_predicted):
        self.training = False
        prediction = self.forward(x_predicted)
        return prediction

    def view_error_development(self):
        print(self.layers)
        plt.plot(range(len(self.error_list)), self.error_list)
        plt.title('Mean Sum Squared Loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.show()

    def test_evaluation(self, input_test, output_test):
        for i, test_element in enumerate(input_test):
            if self.predict(test_element) > self.limit and output_test[i] == 1:
                self.true_positives += 1
            if self.predict(test_element) < self.limit and output_test[i] == 1:
                self.false_negatives += 1
            if self.predict(test_element) > self.limit and output_test[i] == 0:
                self.false_positives += 1
            if self.predict(test_element) < self.limit and output_test[i] == 0:
                self.true_negatives += 1
        print('True positives: ', self.true_positives,
              '\nTrue negatives: ', self.true_negatives,
              '\nFalse positives: ', self.false_positives,
              '\nFalse negatives: ', self.false_negatives,
              '\nAccuracy: ',
              (self.true_positives + self.true_negatives) /
              (self.true_positives + self.true_negatives +
               self.false_positives + self.false_negatives))
