import string
from PIL import Image
import glob
import numpy as np

from data.imgData import ImageData

class ImgLoader():
    def load_images_with_labels(labels, path, width, height):
        image_list = []

        for label in labels:
            for filename in glob.glob(path + '/' + label + '/*.jpg'):  # assuming gif
                im = Image.open(filename)
                im = im.resize((width, height))
                image_list.append(ImageData(im, label))

        return image_list

    def load_image(path,label, name,width,height):
        im = Image.open(path + '/' + label + '/'+ name + '.jpg')
        im = im.resize((width, height))

        return ImageData(im, label)

