import numpy as np
import matplotlib.pyplot as plt

np.random.seed(42)

cat_images = np.random.randn(700, 2) + np.array([0, -3])
mouse_images = np.random.randn(700, 2) + np.array([3, 3])
dog_images = np.random.randn(700, 2) + np.array([-3, 3])

feature_set = np.vstack([cat_images, mouse_images, dog_images])

labels = np.array([0]*700 + [1]*700 + [2]*700)

example_data = np.zeros((2100, 3))

for i in range(2100):
    example_data[i, labels[i]] = 1

plt.figure(figsize=(10,7))
plt.scatter(feature_set[:,0], feature_set[:,1], c=labels, cmap='plasma', s=100, alpha=0.5)
plt.show()

def sigmoid(x):
    return 1/(1+np.exp(-x))

def sigmoid_der(x):
    return sigmoid(x) *(1-sigmoid (x))

def softmax(A):
    expA = np.exp(A)
    return expA / expA.sum(axis=1, keepdims=True)

instances = feature_set.shape[0]
attributes = feature_set.shape[1]
hidden_nodes = 4
output_labels = 3

weight_input = np.random.rand(attributes,hidden_nodes)
bias_input = np.random.randn(hidden_nodes)

weight_outputs = np.random.rand(hidden_nodes,output_labels)
bias_output = np.random.randn(output_labels)
lr = 10e-4

error_cost = []

for epoch in range(50000):
############# feedforward

    # Phase 1
    sum_weights_inputs_bias = np.dot(feature_set, weight_input) + bias_input
    activation_function_inputs = sigmoid(sum_weights_inputs_bias)

    # Phase 2
    sum_weights_output_bias = np.dot(activation_function_inputs, weight_outputs) + bias_output
    activation_function_output = softmax(sum_weights_output_bias)

########## Back Propagation

########## Phase 1

    delta_cost_activation_layer2 = activation_function_output - example_data
    delta_output_weights = activation_function_inputs

    dcost_wo = np.dot(delta_output_weights.T, delta_cost_activation_layer2)

    dcost_bo = delta_cost_activation_layer2

########## Phases 2

    dzo_dah = weight_outputs
    dcost_dah = np.dot(delta_cost_activation_layer2 , dzo_dah.T)
    dah_dzh = sigmoid_der(sum_weights_inputs_bias)
    dzh_dwh = feature_set
    dcost_wh = np.dot(dzh_dwh.T, dah_dzh * dcost_dah)

    dcost_bh = dcost_dah * dah_dzh

    # Update Weights ================

    weight_input -= lr * dcost_wh
    bias_input -= lr * dcost_bh.sum(axis=0)

    weight_outputs -= lr * dcost_wo
    bias_output -= lr * dcost_bo.sum(axis=0)

    if epoch % 200 == 0:
        loss = np.sum(-example_data * np.log(activation_function_output))
        print('Loss function value: ', loss)
        error_cost.append(loss)